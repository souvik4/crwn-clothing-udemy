import {useState} from 'react';
import { createAuthUserWithEmailAndPassword, createUserDocumentFromAuth } from '../../utils/firebase/firebase.utils';
import FormInput from '../form-input/form-input.component';
import Button from '../button/button.component';
import { SignUpContainer } from './sign-up-form.styles';

const defaultFromFields = {
    displayName: '',
    email: '',
    password:'',
    confirmPassword: ''
}

const SignUpForm = () => {
    const [formField, setFormField] = useState(defaultFromFields);
    const {displayName, email, password, confirmPassword} = formField;
 

    const resetFormFields = () => {
        setFormField(defaultFromFields)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        if(password !== confirmPassword){
            alert("password not matched");
            return;
        }
        try{
            const {user} = await createAuthUserWithEmailAndPassword(email, password);
           
            
            await createUserDocumentFromAuth(user, {displayName});
            resetFormFields();

        }catch(error){
            if(error.code === 'auth/email-already-in-use'){
                alert('email is already used')
            }
            else{
                console.log("user creation an error", error);
            }
        }
    }
  
    const handleChange = (event) => {
        const {name, value} = event.target;
        setFormField({...formField, [name]:value })
    }

    return(
        <SignUpContainer>
            <h2>Don't have an account?</h2>
            <span>SignUp with Your Email And Password</span>
            <form onSubmit={handleSubmit}>
                <FormInput label="Display Name" type='text' required onChange={handleChange} name='displayName' value={displayName}/>

                <FormInput label="Email" type='email' required onChange={handleChange} name='email' value={email}/>

                <FormInput label="Password" type='password' required onChange={handleChange} name='password' value={password}/>

                <FormInput label="Confirm Password" type='password' required onChange={handleChange} name='confirmPassword' value={confirmPassword}/>

                <Button type="submit">Sign Up</Button>
            </form>
        </SignUpContainer>
    )
}

export default SignUpForm;